extends Area2D

signal interaction

var consumed = false
onready var level = get_parent()

func _physics_process(delta):
	if consumed:
		return
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.name == "Player" and level.have_key:
			consumed = true
			emit_signal("interaction")

