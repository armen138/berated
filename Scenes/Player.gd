extends KinematicBody2D

onready var level = get_parent()
const UP = Vector2(0, -1)
var movement_speed = 220
var gravity = 400
var gravity_suspended = false
var move_velocity = Vector2(0, 0)
var dead = false
var time_of_death = 0
var spawn_position = Vector2(180, 192)

func die():
	if not dead:
		$animate.play("die")
		dead = true
		time_of_death = OS.get_ticks_msec()
		$die.play()

func respawn():
	dead = false
	position = spawn_position
	time_of_death = 0
	$animate.play("spawn")

func _physics_process(delta):
	var now = OS.get_ticks_msec()
	if dead and now - time_of_death > 500:
		respawn()
	if level.play_mode == level.play_modes.PLAY and !dead:
		move_and_slide(move_velocity, UP)
		if Input.is_action_pressed('left'):
			$PlayerSprite.flip_h = true
			$PlayerSprite/Cannon.flip_h = true
			move_velocity.x = -movement_speed
		elif Input.is_action_pressed('right'):
			move_velocity.x = movement_speed
			$PlayerSprite.flip_h = false
			$PlayerSprite/Cannon.flip_h = false
		else:
			move_velocity.x = 0
		if Input.is_action_just_pressed('jump') and is_on_floor():
			move_velocity.y = -1600
			$jump.play()
		if Input.is_action_just_pressed('fire'):
			var projectile = load("res://Scenes/Projectile.tscn").instance()
			if $PlayerSprite.flip_h:
				projectile.position = position + Vector2(-24, 10)
				projectile.fire(Vector2(-1, 0))
			else:
				projectile.position = position + Vector2(24, 10)
				projectile.fire(Vector2(1, 0))
			get_parent().add_child(projectile)
			$fire.play()
		if abs(move_velocity.x) > 0:
			$PlayerSprite.play("walk")
		if abs(move_velocity.y) > abs(move_velocity.x) and !is_on_floor():
			if move_velocity.y < 0:
				$PlayerSprite.play("jump")
			else:
				$PlayerSprite.play("fall")
		if move_velocity.x == 0 and is_on_floor():
			$PlayerSprite.play("idle")
		if move_velocity.y < gravity and !gravity_suspended:
			move_velocity.y += gravity / 3

