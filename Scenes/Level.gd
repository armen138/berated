extends Node2D

var have_key = false
var coins = 0
var progress = ""
enum play_modes {
	STORY,
	PLAY
}

var story = {
	"NPC": """I don't think you finished writing my [wave][b]dialogue![/b][/wave]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.""",
	"NPC2": """Something terrible has happened to your game, [b]Armen![/b]
It looks like your subconcious fears have been leaked into the code, and you'll have to fight your worst nightmares to get out and fix it: [shake][i]bad reviews![/i][/shake] These little monsters suddenly started appearing, saying mean things!""",
	"incomplete_response": "Ehm... [i]veni vidi vici?[/i]",
	"win": "It wasn't easy, being chased around by bad reviews, but I made it! And I even picked up a few coins along the way! I can already feel that I'm waking up to a new day, full of [i][wave]positive vibes[/wave][/i]. I'm getting up, removing all the [b][shake]fears[/shake][/b] and [b][shake]doubts[/shake][/b] from the game, and submit! Or... perhaps it wouldn't be fair to the player, to remove that which inspired the game in the first place."
}

var play_mode = play_modes.STORY

func _ready():
	$HUD/Anchor/PanelAnimator.play("show")
	$NPC/InteractionTrigger.connect("interaction", self, "on_npc_story_trigger")
	$NPC2/InteractionTrigger.connect("interaction", self, "on_npc_story_trigger")
	$Key/KeyCollector.connect("interaction", self, "on_key_pickup")
	$KeyHole.connect("interaction", self, "on_unlock_keyhole")

func _process(delta):
	if Input.is_action_just_pressed('ui_accept'):
		if play_mode != play_modes.PLAY:
			if $HUD/Anchor/PanelAnimator.is_playing():
				$HUD/Anchor/PanelAnimator.seek(10.0)
			else:
				if progress == "win":
					get_tree().change_scene("res://Scenes/Credits.tscn")
				else:
					play_mode = play_modes.PLAY
				$HUD/Anchor/PanelAnimator.play("hide")

func on_flag_pickup():
	$sfx/win.play()
	on_story_trigger("win")

func on_coin_pickup():
	coins += 1
	$HUD/CoinCounter/CoinLabel.text = "Coins: " + str(coins)

func on_unlock_keyhole():
	$TileMap.set_cell(53, 24, -1)
	$TileMap.set_cell(53, 25, -1)
	$TileMap.set_cell(53, 26, -1)
	$sfx/door.play()

func on_key_pickup():
	have_key = true
	$HUD/have_key.visible = true
	$sfx/key.play()

func on_story_trigger(id):
	print("Story trigger ", id)
	progress = id
	if play_mode != play_modes.STORY:
		play_mode = play_modes.STORY
		$HUD/Anchor/Panel/npc_portrait.hide()
		$HUD/Anchor/Panel/portrait.show()
		$HUD/Anchor/Panel/label.bbcode_text = story[id]
		$HUD/Anchor/PanelAnimator.play("show")
		$sfx/interact.play()
	

func on_npc_story_trigger(id):
	print("Story trigger ", id)
	progress = id
	if play_mode != play_modes.STORY:
		play_mode = play_modes.STORY
		$HUD/Anchor/Panel/portrait.hide()
		$HUD/Anchor/Panel/npc_portrait.show()
		$HUD/Anchor/Panel/label.bbcode_text = story[id]
		$HUD/Anchor/PanelAnimator.play("show")
		$sfx/interact.play()
