extends Area2D

signal interaction

var consumed = false

func _ready():
	connect("interaction", get_parent().get_parent(), "on_flag_pickup")

func _physics_process(delta):
	if consumed:
		return
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.name == "Player":
			consumed = true
			emit_signal("interaction")
