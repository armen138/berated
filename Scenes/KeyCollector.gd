extends Area2D

signal interaction

var consumed = false

func _physics_process(delta):
	if consumed:
		return
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.name == "Player":
			consumed = true
			emit_signal("interaction")
			get_parent().hide()
