extends KinematicBody2D

const UP = Vector2(0, -1)
var move_velocity = Vector2(0, 0)
var speed = 7
var lifetime = 500
var birth = 0
var kill_list = [
	"Troll",
	"Karen",
	"Gremlin",
	"Cyclops"
]

func _ready():
	birth = OS.get_ticks_msec()
	
func fire(direction):
	move_velocity = direction * speed;

func end_path():
	$Sprite.hide()
	$Particles2D.emitting = false
	$CollisionShape2D.disabled = true

func _physics_process(delta):
	var now = OS.get_ticks_msec()
	if now - birth > lifetime * 2:
		queue_free()
	elif now - birth > lifetime:
		end_path()
	else:
		var collision = move_and_collide(move_velocity)
		if collision:
			end_path()
			$Explode.emitting = true
			move_velocity = Vector2(0, 0)
			if collision.collider.name in kill_list:
				collision.collider.die()
				$die.play()
			$impact.play()
