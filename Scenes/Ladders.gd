extends Area2D


func _physics_process(delta):
	var bodies = get_overlapping_bodies()
	var have_player = false
	for body in bodies:
		if body.name == "Player":
			body.gravity_suspended = true
			have_player = true
	if not have_player:
		get_parent().get_parent().get_node("Player").gravity_suspended = false
