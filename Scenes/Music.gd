extends Node

func _on_woods_body_entered(body):
	if body.name == "Player":
		$woods/music.play()

func _on_woods_body_exited(body):
	if body.name == "Player":
		$woods/music.stop()

func _on_caves_body_entered(body):
	if body.name == "Player":
		$caves/music.play()

func _on_caves_body_exited(body):
	if body.name == "Player":
		$caves/music.stop()

func _on_ice_body_entered(body):
	if body.name == "Player":
		$ice/music.play()

func _on_ice_body_exited(body):
	if body.name == "Player":
		$ice/music.stop()
