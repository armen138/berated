extends KinematicBody2D

onready var level = get_parent()
const UP = Vector2(0, -1)
var movement_speed = 170
var gravity = 400
var move_velocity = Vector2(0, 0)
var direction = "left"
var last_change = 0
var dead = false
var time_of_death = 0

func die():
	$animate.play("die")
	dead = true
	time_of_death = OS.get_ticks_msec()

func _physics_process(delta):
	var now = OS.get_ticks_msec()
	if dead:
		if now - time_of_death > 500:
			queue_free()
	else:
		if level.play_mode == level.play_modes.PLAY:
			move_and_slide(move_velocity, UP)
			if direction == 'left':
				$TrollSprite.flip_h = true
				move_velocity.x = -movement_speed
			elif direction == 'right':
				move_velocity.x = movement_speed
				$TrollSprite.flip_h = false
			else:
				move_velocity.x = 0
			if abs(move_velocity.x) > 0:
				$TrollSprite.play("walk")
			if abs(move_velocity.y) > abs(move_velocity.x) and !is_on_floor():
				if move_velocity.y < 0:
					$TrollSprite.play("jump")
				else:
					$TrollSprite.play("fall")
			if move_velocity.x == 0 and is_on_floor():
				$TrollSprite.play("idle")
			if is_on_wall() and last_change + 100 < OS.get_ticks_msec():
				last_change = now
				if direction == "left":
					direction = "right"
				else:
					direction = "left"
			if move_velocity.y < gravity:
				move_velocity.y += gravity / 3
			for i in get_slide_count():
				var collision = get_slide_collision(i)
#				print("Collided with: ", collision.collider.name)
				if collision.collider.name == "Player":
					collision.collider.die()
