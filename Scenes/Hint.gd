extends Area2D

export var hint = "hint"
var have_player = false
var last_player_contact = 0
var hidden = true
# Called when the node enters the scene tree for the first time.
func _ready():
	$tip.bbcode_text = hint

func _physics_process(delta):
	var now = OS.get_ticks_msec()
	var bodies = get_overlapping_bodies()
	have_player = false
	for body in bodies:
		if body.name == "Player":
			if !have_player and hidden:
				$AnimationPlayer.play("show")
				hidden = false
				have_player = true
			last_player_contact = now
	if !have_player and !hidden and now - last_player_contact > 1000:
		$AnimationPlayer.play("hide")
		print("Hide hint")
		hidden = true

