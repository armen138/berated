Berated is a game by Armen (armen138.com)

The following assets were used:

Under cc0:
Pixel font by Kenney.nl
Platformer tiles by Kenney.nl
Sound Effects by SubspaceAudio (https://opengameart.org/content/512-sound-effects-8-bit-style)

Under cc-by:
Platformer game music by CodeManu (https://opengameart.org/content/platformer-game-music-pack)

Additionally, characters in this game were created with Kenney's Creature-Mixer (https://kenney.itch.io/creature-mixer), and in accordance with its license, I hereby confirm my agreement to never use this game to promote or create non-fungible tokens (NFT's), or any crypto-currency or anything related to blockchains.
